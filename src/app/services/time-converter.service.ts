import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TimeConverterService {

  constructor() { }

  public timeConvert(value: number): string {
    let time = new Date(value * 60 * 1000).toISOString().substr(11, 5);
    if(time.startsWith('00') || time.startsWith('01')) {
      return time + ' hour';
    }
    return time + ' hours';
  }
}
