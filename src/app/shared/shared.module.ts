import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ButtonComponent } from './components/button/button.component';
import { InfoComponent } from './components/info/info.component';
import { SearchComponent } from './components/search/search.component';

const componentsArray = [
  ButtonComponent,
  InfoComponent,
  SearchComponent
];

@NgModule({
  declarations: [...componentsArray],
  imports: [
    CommonModule,
    FontAwesomeModule
  ],
  exports: [...componentsArray, CommonModule, FontAwesomeModule]
})
export class SharedModule { }
