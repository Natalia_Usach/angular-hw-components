import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {

  @Input() isEditable!: boolean;
  @Input() courses: any[] = [];
  @Output() newItemEvent = new EventEmitter<string>();

  faEdit = faEdit;
  faTrash = faTrash;
  btnText: string = "Show course";

  constructor() { 
  }

  ngOnInit(): void {
  }
}
