import { Component, OnInit } from '@angular/core';
import * as mockedCourseList from './../../../mocks/mocks.json';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})

export class CoursesComponent implements OnInit {

  courseList: any;
  titleText: string = 'Your list is empty';
  infoText: string = `Please use the 'Add new course' button to add your first course`;
  btnText: string = 'Add new course';
  editable: boolean = true;
  
  constructor() { 
    this.courseList = Object.values(mockedCourseList)[3];
  }

  ngOnInit(): void {
  }

}
