import { Component, Input, OnInit } from '@angular/core';
import { TimeConverterService } from 'src/app/services/time-converter.service';

@Component({
  selector: 'app-course-card',
  templateUrl: './course-card.component.html',
  styleUrls: ['./course-card.component.css'],
  providers: [TimeConverterService]
})

export class CourseCardComponent implements OnInit {
  @Input() courseItem: any;

  constructor(private timeConverterService: TimeConverterService) {
  }

  ngOnInit(): void {
  }

  public transformTime(value: number): string {
    return this.timeConverterService.timeConvert(value);
  }
}
